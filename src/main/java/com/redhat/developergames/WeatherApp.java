package com.redhat.developergames;

import com.redhat.developergames.model.Weather;
import com.redhat.developergames.repository.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;


@SpringBootApplication
@RestController
public class WeatherApp {

   @Autowired
   WeatherRepository weatherRepository;

   @GetMapping("/")
   public String index() {
      return "Greetings from Spring Boot with Data Grid!";
   }

   @GetMapping("/weather/{location}")
   public Object getByLocation(@PathVariable String location, HttpServletRequest request) {
      Weather weather = weatherRepository.getByLocation(location);
      if (weather == null) {
         return String.format("Weather for location %s not found", location);
      }
      request.getSession().setAttribute("latest",weather);
      return weather;
   }

   @GetMapping("/latest")
   public Object latestLocation(HttpServletRequest request) {
      if( Objects.nonNull(request.getSession().getAttribute("latest")) ){
         Weather w = (Weather)request.getSession().getAttribute("latest");
         return w;
      }else{
         return "ops, you don't have session yet!";
      }
   }

   public static void main(String... args) {
      new SpringApplicationBuilder().sources(WeatherApp.class).run(args);
   }
}
