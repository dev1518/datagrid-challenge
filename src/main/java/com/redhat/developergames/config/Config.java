package com.redhat.developergames.config;

import org.infinispan.spring.remote.session.configuration.EnableInfinispanRemoteHttpSession;
import org.springframework.context.annotation.Configuration;

@EnableInfinispanRemoteHttpSession(cacheName = "sessions")
@Configuration
public class Config {

}