package com.redhat.developergames.config;

import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.configuration.ConfigurationBuilder;
import org.infinispan.client.hotrod.configuration.NearCacheMode;
import org.infinispan.client.hotrod.impl.ConfigurationProperties;
import org.infinispan.commons.marshall.JavaSerializationMarshaller;
import org.infinispan.spring.remote.provider.SpringRemoteCacheManager;
import org.infinispan.spring.starter.remote.InfinispanRemoteCacheCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.net.URI;
import java.net.URISyntaxException;

@Configuration
@EnableCaching
public class InfinispanConfiguration {

   @Value("${infinispan.client.hotrod.server}")
   private String host;

   @Value("${infinispan.client.hotrod.auth.username}")
   private String username;

   @Value("${infinispan.client.hotrod.auth.password}")
   private String password;
   private static final Logger LOG = LoggerFactory.getLogger(InfinispanConfiguration.class);

   @Bean
   public RemoteCacheManager remote() {
      // Create a client configuration connecting to a local server
      ConfigurationBuilder builder = new ConfigurationBuilder();
      builder.addServer().host(host).port(ConfigurationProperties.DEFAULT_HOTROD_PORT).
              security().authentication().password(password).username(username).
              marshaller(new JavaSerializationMarshaller())
              .addJavaSerialAllowList("com.redhat.developergames.model.*");
      //builder.nearCache().mode(NearCacheMode.INVALIDATED).maxEntries(20).cacheNamePattern("near-.*");

      // Connect to the server
      RemoteCacheManager cacheManager = new RemoteCacheManager(builder.build());
      return cacheManager;
   }


   @Bean
   @Order(Ordered.HIGHEST_PRECEDENCE)
   public InfinispanRemoteCacheCustomizer caches() {
      LOG.info("Creating InfinispanRemoteCacheCustomizer -LOG");
      return b -> {
         // Configure the weather cache to be created if it does not exist in the first call
         URI weatherCacheConfigUri = cacheConfigURI("weatherCache.xml");
         b.remoteCache("weather")
                 .configurationURI(weatherCacheConfigUri).create();
      };
   }

   @Bean
   @Order(Ordered.HIGHEST_PRECEDENCE)
   public InfinispanRemoteCacheCustomizer cacheSession() {
      LOG.info("Creating InfinispanRemoteCacheCustomizer SESSION -LOG");
      return b -> {
         // Configure the weather cache to be created if it does not exist in the first call
         URI weatherCacheConfigUri = cacheConfigURI("sessionsCache.xml");
         b.remoteCache("sessions")
                 .configurationURI(weatherCacheConfigUri).create();
      };
   }

   private URI cacheConfigURI(String cacheConfigFile) {
      URI cacheConfigUri;
      try {
         cacheConfigUri = this.getClass().getClassLoader().getResource(cacheConfigFile).toURI();
      } catch (URISyntaxException e) {
         throw new RuntimeException(e);
      }
      return cacheConfigUri;
   }

}
